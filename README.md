# HeritagePhone

## Main Controller Script

- Entrypoint: `phonecode/main.py`
- Configuration: `phonecode/config.py`
- Deployment Info: `deployment/`
- Main Deployment Script: `deployment/deploy.sh`


## Deployment

The main deployment script simply deploys over SSH into the `/home/pi/phonecode/` folder.

You'll probably want to set up a SSH config profile,
otherwise you'll be constantly asked for the password during deployment.

First time installation (or if the service file changes) is as below,
but you should probably ensure the system is updated first.
The setup script will not do a `apt upgrade` for you.

```bash
# On your laptop
# Copy all files to pi
./deployment/deploy.sh

# SSH into the pi and run:
cd /home/pi/phonecode/
sudo ./deploy/setup_pi.sh

```


## Connection information

At the Hackerspace, the IP address of the Pi is `192.168.1.49` on the `bhack` Wi-Fi.


## Useful links

This page helped me get set up on the Pi3 for Serial comms: https://www.circuits.dk/setup-raspberry-pi-3-gpio-uart/
