"""It's... History on The Line!

Pick up the receiver, dial in a number and get extra information about Ballarat's past.
"""
import os.path
import random
from typing import Optional
import logging
import subprocess
from datetime import datetime

import serial

import config

HANDSET_DOWN = False
HANDSET_UP = True

default_state = {
    'handset': HANDSET_DOWN,  # Is the handset up?
    'handset-up-time': 0,  # When was the handset raised
    'dialed': "",  # What numbers have been dialed?
    'mplayer-started': None,
    'player': None,  # Actual mplayer process instance
    'last_played_file': None,
    'next_filename': None,
}

PHONE_STATE = default_state.copy()


def reset_state():
    global PHONE_STATE
    PHONE_STATE = default_state.copy()


def handset_up():
    PHONE_STATE['handset'] = HANDSET_UP


def handset_down():
    PHONE_STATE['handset'] = HANDSET_DOWN


def setup_logging(log_name: str, filename: str, debug: bool):
    logger = logging.getLogger(log_name)
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler(filename)
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()

    if debug:
        ch.setLevel(logging.DEBUG)
    else:
        ch.setLevel(logging.ERROR)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s,%(name)s,%(levelname)s,%(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    # add the handlers to the logger
    logger.addHandler(fh)
    logger.addHandler(ch)

    logger.warning(f"Log filename: {os.path.abspath(filename)}")
    return logger


def main():
    import sys
    if len(sys.argv) > 1:
        debug_mode = ('debug' in sys.argv)
    else:
        debug_mode = False

    if debug_mode:
        # when debugging, we put log locally, as we probably can't put logs in /var/logs
        log_filename = "statistics.log"
    else:
        log_filename = config.LOG_FILENAME
    events = setup_logging("STATS", log_filename, debug=debug_mode)
    run_program(events)


def log_file_complete(events):
    end = datetime.now()
    if PHONE_STATE['mplayer-started']:
        time_taken = (end - PHONE_STATE['mplayer-started']).total_seconds()
        events.info(f"TIME_PLAYED,{PHONE_STATE['last_played_file']},{time_taken}s")


def _check_player_finished(events, time_of_last_command):
    if PHONE_STATE['player']:
        poll = PHONE_STATE['player'].poll()
        # events.debug(f"Player poll: {poll}")
        if poll is not None:
            events.debug(f"Player finished: {poll}")
            # Player was active, but has finished now
            log_file_complete(events)
            kill_player_if_present(events, PHONE_STATE)
            PHONE_STATE['last-useful-action'] = datetime.now()
            time_of_last_command = datetime.now()
            # If there is a next filename, play it now
            if PHONE_STATE['next_filename']:
                play_file(events, PHONE_STATE['next_filename'], PHONE_STATE)
                PHONE_STATE['next_filename'] = None
    return time_of_last_command


def set_volume(events):
    events.info("Setting volume to 100%")
    cmd = "sudo amixer set Headphone,0 100%"
    p = subprocess.Popen(
        cmd.split(),
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    events.info(p.communicate())


def run_program(events):
    events.info("PROGRAM,STARTED")
    try:
        with serial.Serial(
                config.SERIAL_PORT, config.BAUD_RATE, timeout=config.SERIAL_TIMEOUT) as ser:
            print("Connected")
            events.info("CONNECTED,SERIAL")
            try:
                time_of_last_command = datetime.now()
                PHONE_STATE['last-useful-action'] = datetime.now()

                ser.write(config.STARTUP_COMMAND)

                set_volume(events)

                line_chars = []
                while True:
                    time_of_last_command = _check_player_finished(
                        events, time_of_last_command)
                    time_of_last_command = _handle_user_inaction(
                        events, time_of_last_command)
                    time_of_last_command, line_chars = _handle_inputs(
                        events, ser, line_chars, time_of_last_command)
            except Exception as e:
                events.error(f"Error of type {type(e)}: {e}")
            finally:
                events.warning("SERIAL,DISCONNECTING")
    except Exception as e:
        events.error(f"Error of type {type(e)}: {e}")
    finally:
        events.warning("PROGRAM,CLOSED")
        print("Exited")


def _handle_inputs(events, ser, line_chars, time_of_last_command):
    for c in ser.read():
        time_of_last_command = datetime.now()
        c = chr(c)
        line_chars.append(c)
        if c == '\n':
            line = str.join("", line_chars)
            line_chars = []
            output = None
            try:
                output = handle_input(line, events)
            except Exception as e:
                events.error(f"INPUT_ERROR,{line},{type(e)},{e}")
                reset_state()
                raise
            if output:
                events.info(f"Sending: {output}")
                ser.write(output.encode())
            break
    return time_of_last_command, line_chars


def _handle_user_inaction(events, time_of_last_command):
    if PHONE_STATE['player']:
        # Currently playing, so don't interrupt
        time_of_last_command = datetime.now()
        return time_of_last_command
    handset_up = PHONE_STATE['handset'] == HANDSET_UP
    time_since_user_action = (datetime.now() - time_of_last_command).total_seconds()
    time_since_useful_action = (datetime.now() - PHONE_STATE['last-useful-action']).total_seconds()
    # events.debug(f"LOOP,{handset_up},{time_since_useful_action},{time_since_user_action}")
    if handset_up and (time_since_user_action > config.USER_NO_INPUT_DELAY_SECONDS):
        handle_user_timeout(PHONE_STATE, events)
        time_of_last_command = datetime.now()
        PHONE_STATE['last-useful-action'] = datetime.now()
    if handset_up and (time_since_useful_action > config.USER_OVERALL_INPUT_DELAY_SECONDS):
        handle_user_timeout(PHONE_STATE, events)
        time_of_last_command = datetime.now()
        PHONE_STATE['last-useful-action'] = datetime.now()

    if not handset_up and (time_since_user_action > config.USER_NO_INPUT_DELAY_SECONDS):
        # If the user hanged up, and it's been a while, clear the dialed digits
        if PHONE_STATE['dialed']:
            events.info("Clearing digits as user hangup too long")
            PHONE_STATE['dialed'] = ''
    return time_of_last_command


def handle_user_timeout(state, events):
    """User took too long to do anything, so maybe they don't understand?"""
    events.info("User took too long to do *anything*, so we are playing a random file")
    filename = random.choice(list(config.sound_files.values()))
    play_file(events, filename, state)
    state['last-useful-action'] = datetime.now()


def handle_input(command, events) -> Optional[bytes]:
    command = command.strip()

    if not command:
        return

    # Log "everything" at DEBUG level
    events.debug("INPUT,{}".format(command))

    command_key = command
    if command.startswith("DIN"):
        command_key = "DIN"

    if command_key not in actions:
        events.warning(f"Unknown command key: '{command_key}'")
        return

    PHONE_STATE['last-useful-action'] = datetime.now()
    command_handler = actions[command_key]
    output = command_handler(PHONE_STATE, events, command)

    return output


def find_nearest_filename(digits, events):
    if len(digits) < 4:
        # Not enough digits
        return

    if digits == '5555':
        # Wrong number man!
        return None

    scores = {}
    for digit_pattern in config.sound_files.keys():
        score = abs(len(digits) - len(digit_pattern)) * 10
        for d1, d2 in zip(digit_pattern, digits):
            score += abs(ord(d1) - ord(d2))
        scores[digit_pattern] = score

    if digits[0] == config.PENALTY_START_DIGIT:
        events.info("Penalising first digit")
        # Penalise the first digit
        for digit_pattern in config.sound_files.keys():
            scores[digit_pattern] += config.PENALTY_START_DIGIT_SCORE

    events.info(scores)
    best_key = sorted(scores, key=lambda k: scores[k])[0]

    if scores[best_key] < config.NEAREST_SCORE_THRESHOLD:
        events.info(f"Exact match failed, best match was: {best_key}")
        filename = config.sound_files[best_key]
        return filename

    # Nothing close enough
    return None


def check_digits_and_play_file(state, events):
    digits = state['dialed']

    filename = None
    if PHONE_STATE['handset'] == HANDSET_DOWN:
        # Do not play if handset is down
        return

    # Check if we have a file
    if digits in config.sound_files:
        filename = config.sound_files[digits]
        events.info(f"Filename: {filename}")
    else:
        filename = find_nearest_filename(digits, events)

    if filename is None:
        events.warning(f"Unknown set of digits: {digits}")

        if len(config.WRONG_NUMBER_FILENAMES) == 0:
            events.info("Not playing a wrong number tone as there are none")
            return
        elif len(digits) >= 4:
            # Choose a random wrong number filename
            filename = random.choice(config.WRONG_NUMBER_FILENAMES)
            events.info(f"Playing a wrong number sound: {filename}")

    if filename:
        PHONE_STATE['next_filename'] = filename
        play_file(events, config.DIALTONE_FILENAME, state)


def play_file(events, filename, state):
    # First, check if the file is still going
    if state['player']:
        if state['player'].poll() is None:
            # Still playing a sound, so don't play another one
            events.info("Still playing some other file")
            return
    full_path = f"{config.SOUND_FOLDER}/{filename}"
    events.info(f"PLAYING,{full_path}")
    state['last_played_file'] = full_path
    # Play the file
    mplayer_call = ['aplay'] + config.MPLAYER_ARGS + [full_path]
    events.debug(f"Mplayer: {mplayer_call}")
    PHONE_STATE['mplayer-started'] = datetime.now()
    state['player'] = subprocess.Popen(
        mplayer_call,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)


def add_digit(state, events, command):
    """Adds the dialed digit to the set of numbers.

    If we have enough digits, play a sound
    """
    digit = command[3:]
    events.info(f"Adding digit: {digit}")
    state['dialed'] += digit

    print(state['dialed'])

    check_digits_and_play_file(state, events)


def handset_hangup(state, events, command):
    """
    Hangup the handset, and kill any playing sounds
    :param state:
    :param events:
    :param command:
    :return:
    """
    state['handset'] = HANDSET_DOWN
    state['dialed'] = ''
    kill_player_if_present(events, state)
    state['player'] = None
    return "2"


def kill_player_if_present(events, state):
    """
    Kill the player if it's present
    :param events:
    :param state:
    :return:
    """
    # Stop any running music processes
    if state['player']:
        state['player'].kill()
        events.info("Killed playing sound")
        log_file_complete(events)
        state['player'] = None


def handset_lifted(state, events, command):
    """
    When handset lifted, play the file if conditions are met
    :param state:
    :param events:
    :param command:
    :return:
    """
    state['handset'] = HANDSET_UP

    check_digits_and_play_file(state, events)


actions = {
    "DIN": add_digit,
    "ONHO": handset_hangup,
    "OFHO": handset_lifted,
}

if __name__ == "__main__":
    main()
