SERIAL_PORT = "/dev/ttyS0"
BAUD_RATE = 9600
# Serial timeout, we want this low so we can poll for other things
SERIAL_TIMEOUT = 4
SOUND_FOLDER = "/home/pi/heritagephone/phonecode/files"

INNER_LOOP_DELAY = 0.1
LOG_FILENAME = "/var/log/heritage_phone_statistics.log"

# The extra commands may be needed if you have other audio devices attached to the pi
# (like HDMI with audio)
MPLAYER_ARGS = []  # e.g. for mplayer ["-ao", "alsa:device=hw=1"]

# Timeout for user to not specify any input
USER_NO_INPUT_DELAY_SECONDS = 20
# Timeout for user to fail to do anything useful
USER_OVERALL_INPUT_DELAY_SECONDS = 60

NEAREST_SCORE_THRESHOLD = 20

sound_files = {
    '2136': 'Art Gallery of Ballarat.wav',
    '4667': 'Mining Exchange.wav',
    '9898': 'Town Hall.wav',
    '2157': 'Ballaarat Mechanics_ Institute.wav',
    '6546': 'Eureka.wav',
    '3264': 'Craig_s Royal Hotel.wav',
    '1657': 'Old Ballarat Gaol.wav',
    '2654': 'Civic Hall.wav',
}

STARTUP_COMMAND = b"2"

WRONG_NUMBER_FILENAMES = [
    "Henry Sutton with crackle.wav",
    "chris.wav",
    'Upset lady on the phone.wav'
]

DIALTONE_FILENAME = "dialing.wav"


# Pretty blunt, but if the code starts with this digit, add this penalty to all scores
# Increases the likelihood of a wrong number being chosen
PENALTY_START_DIGIT = '5'
PENALTY_START_DIGIT_SCORE = 15
