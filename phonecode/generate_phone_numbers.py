import random
from random import sample
from itertools import combinations_with_replacement

from config import sound_files

n = len(sound_files)
phone_number_length = 4

numbers = {
    'low': [1, 2, 3],
    'med': [4, 5, 6],
    'high': [7, 8, 9],
}

print(n)

combinations = list(combinations_with_replacement(numbers, phone_number_length))

used_combinations = sample(combinations, n)

for filename, combination in zip(sound_files, used_combinations):
    phone_number = ""

    for c in combination:
        phone_number = phone_number + str(random.choice(numbers[c]))
    print(f"'{phone_number}': '{filename}',")
