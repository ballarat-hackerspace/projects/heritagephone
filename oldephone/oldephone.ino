#define DIAL_COUNT_PIN   2
#define DIAL_START_PIN   3

#define RINGER_IN_1     10
#define RINGER_IN_2     11
#define RINGER_IN_3     12
#define RINGER_IN_4     13

#define HOOK_PIN         9

volatile uint8_t count = 0;
volatile bool counting = false;
volatile bool countComplete = false;

uint32_t lastRingerUpdate = 0;
uint32_t lastRingerStateUpdate = 0;
uint32_t lastRingerState = 1;
bool ringing = false;
uint32_t ringDuration = 1000;

bool offHookState = false;
bool lastOffHookState = false;
uint32_t lastOffHookUpdate = 0;

char dialMap[32] = {
  'X', 'X', 'X', 'X', 'X', '1', '1', '2',
  '2', '3', '3', '4', '4', '5', '5', '6',
  '6', '7', '7', '8', '8', 'X', 'X', 'X',
  'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X'
};

// the setup function runs once when you press reset or power the board
void setup() {
  Serial.begin(9600);

  pinMode(HOOK_PIN, INPUT_PULLUP);

  pinMode(DIAL_COUNT_PIN, INPUT);
  pinMode(DIAL_START_PIN, INPUT);

  attachInterrupt(digitalPinToInterrupt(DIAL_COUNT_PIN), isrDialCount, FALLING);
  attachInterrupt(digitalPinToInterrupt(DIAL_START_PIN), isrDialMarker, CHANGE);

  pinMode(RINGER_IN_1, OUTPUT);
  pinMode(RINGER_IN_2, OUTPUT);
  pinMode(RINGER_IN_3, OUTPUT);
  pinMode(RINGER_IN_4, OUTPUT);

  digitalWrite(RINGER_IN_1, LOW);
  digitalWrite(RINGER_IN_2, LOW);
  digitalWrite(RINGER_IN_3, LOW);
  digitalWrite(RINGER_IN_4, LOW);

  Serial.println("INIT");
}

void isrDialCount() {
  if (counting) {
    count++;
  }
}

void isrDialMarker() {
  if (!digitalRead(DIAL_START_PIN)) {
    counting = true;
    count = 0;
  }
  else {
    counting = false;
    countComplete = true;
  }
}

uint32_t lastUpdate = 0;

// the loop function runs over and over again forever
void loop() {
  uint32_t n = millis();

  if ((n - lastUpdate) > 500) {
    lastUpdate = n;
  }

  // check for a dial completion
  if (countComplete) {
    Serial.print("DIN");
    Serial.println(dialMap[count]);

    countComplete = false;
  }

  // check for on hook
  bool readingState = !digitalRead(HOOK_PIN);

  if (readingState != lastOffHookState) {
    lastOffHookUpdate = n;
  }

  if ((n - lastOffHookUpdate) > 250) {
    if (readingState != offHookState) {
      offHookState = readingState;

      Serial.println(offHookState ? "OFHO" : "ONHO");
    }
  }

  lastOffHookState = readingState;

  // process ringing (if on hook only)
  if (ringing) {
    processRing(n);

    if (
      ((n - lastRingerUpdate) > ringDuration) ||
      offHookState
    ) {
      ringing = false;
      setMotor(0);
    }
  }

  // process serial input
  processSerial();
}

void processRing(uint32_t r) {
  if (r - lastRingerStateUpdate > 40) {
    lastRingerState = (lastRingerState + 1) % 4;

    setMotor(lastRingerState);

    lastRingerStateUpdate = r;
  }
}

void processSerial() {
  while (Serial.available() > 0) {
    char f =  Serial.read();

    if (f == '1') {
      startRing(1000);
    } else if (f == '2') {
      startRing(2000);
    } else if (f == '3') {
      startRing(3000);
    } else if (f == '4') {
      startRing(4000);
    } else if (f == '5') {
      startRing(5000);
    } else if (f == 'X') {
      Serial.println(offHookState ? "OFHO" : "ONHO");
    }
  }
}

void setMotor(uint8_t state) {
  if (state == 1) {
    digitalWrite(RINGER_IN_1, LOW); // left coil attract
    digitalWrite(RINGER_IN_2, HIGH);
  } else if (state == 3) {
    digitalWrite(RINGER_IN_3, LOW); // right coil attract
    digitalWrite(RINGER_IN_4, HIGH);
  } else {
    digitalWrite(RINGER_IN_1, LOW);
    digitalWrite(RINGER_IN_2, LOW);
    digitalWrite(RINGER_IN_3, LOW);
    digitalWrite(RINGER_IN_4, LOW);
  }
}

void startRing(uint32_t duration) {
  if (!offHookState) {
    ringing = true;
    lastRingerUpdate = millis();
    ringDuration = duration;
  }
}
