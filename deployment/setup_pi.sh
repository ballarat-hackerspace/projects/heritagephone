sudo apt update
sudo apt install -y python3-setuptools python3-pip mplayer aplayer
sudo python3 -m pip install -r /home/pi/heritagephone/phonecode/requirements.txt


# Setup the system process
sudo cp /home/pi/heritagephone/deployment/heritagephone.service /etc/systemd/system/heritagephone.service
sudo chmod 664 /etc/systemd/system/heritagephone.service
sudo systemctl daemon-reload
sudo systemctl enable heritagephone
sudo systemctl start heritagephone
