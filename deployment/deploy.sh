#!/usr/bin/env bash

echo "To be run in the top level heritagephone directory"

IP=${1:-192.168.1.49}
CONN=pi@${IP}
echo "Deploying to $CONN"

echo "Copying files"
ssh $CONN mkdir -p /home/pi/heritagephone
scp -r deployment ${CONN}:/home/pi/heritagephone/
rsync --exclude='*.wav' -v -r phonecode ${CONN}:/home/pi/heritagephone/

echo "Complete"
